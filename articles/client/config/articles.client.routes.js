(function () {
  'use strict';

  angular
    .module('articles.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider

   .state('articles', {
        abstract: true,
        url: '/articles',
        template: '<ui-view/>'
      })



     .state('home', {
        url: '/',
        templateUrl: '/modules/articles/client/views/home.client.view.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      })


     .state('find', {
        url: '/find',
        templateUrl: '/modules/articles/client/views/search.client.view.html',
        controller: 'SearchController',
        controllerAs: 'vm',
        resolve: {
          searchResolve: getRow
        }//,
        //data: {
          //keyword: '{{ searchResolve.keyword }}'
        //}
      })


     /*.state('info', {
        url: '/info',
        templateUrl: '/modules/articles/client/views/info.client.view.html',
        controller: 'InfoController',
        resolve:{
		infoResolve: getInfo
	}
      })*/


     .state('articles.list', {
        url: '',
        templateUrl: '/modules/articles/client/views/list-articles.client.view.html',
        controller: 'ArticlesListController',
        controllerAs: 'vm'
      })


      .state('articles.view', {
        url: '/:articleId',
        templateUrl: '/modules/articles/client/views/view-article.client.view.html',
        controller: 'ArticlesController',
        controllerAs: 'vm',
        resolve: {
          articleResolve: getArticle
        },
        data: {
          pageTitle: '{{ articleResolve.title }}'
        }
      });
  }

  getArticle.$inject = ['$stateParams', 'ArticlesService'];

  
  function getArticle($stateParams, ArticlesService) {
    return ArticlesService.get({
      articleId: $stateParams.articleId
    }).$promise;
  }


 function getRow($stateParams, SearchService) {
    return SearchService.get({
      keyword: $stateParams.keyword
    }).$promise;
  }

 function getInfo(InfoService){
	return InfoService.get().$promise;

 }
}());
